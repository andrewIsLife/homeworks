/*

  Задание 1.

  Написать функцию которая будет задавать СЛУЧАЙНЫЙ цвет для фона.
  Каждая перезагрузка страницы будет с новым цветом.
  Для написания используйте функцию на получение случайного целого числа,
  между минимальным и максимальным значением (Приложена снизу задания)

  + Бонус, повесить обработчик на кнопку через метод onClick
  + Бонус, использовать 16-ричную систему исчесления и цвет HEX -> #FFCC00
  + Бонус выводить полученый цвет по центру страницы.
  
  Необходимо создать блок через createElement задать ему стили через element.style
  и вывести через appendChild или insertBefore

  Необходимые материалы:
    Math.Random (Доки): https://developer.mozilla.org/uk/docs/Web/JavaScript/Reference/Global_Objects/Math/random
    function getRandomIntInclusive(min, max) {
      min = Math.ceil(min);
      max = Math.floor(max);
      return Math.floor(Math.random() * (max - min + 1)) + min;
    }
    __
    Работа с цветом:
    Вариант 1.
      Исользовать element.style.background = 'rgb(r,g,b)';
      где r,g,b случайное число от 0 до 255;

    Вариант 2.
      Исользовать element.style.background = '#RRGGBB';
      где, RR, GG, BB, значние цвета в 16-ричной системе исчесления
      Формирование цвета в вебе: https://ru.wikipedia.org/wiki/%D0%A6%D0%B2%D0%B5%D1%82%D0%B0_HTML
      Перевод в 16-ричную систему исчесления делается при помощи
      метода Number.toString( 16 ) https://www.w3schools.com/jsref/jsref_tostring_number.asp,

      var myNumber = '251'
      myNumber.toString(16) // fb

*/
//-----app------
var app = function() {
    var myNumberR = getRandomIntInclusive(0, 255);
    var myNumberG = getRandomIntInclusive(0, 255);
    var myNumberB = getRandomIntInclusive(0, 255);
    var r = myNumberR.toString(16);
    var g = myNumberG.toString(16);
    var b = myNumberB.toString(16);
    if (r.length == 1) r = r + '' + r;
    if (g.length == 1) g = g + '' + g;
    if (b.length == 1) b = b + '' + b;
    //----------createStyleBacground-------------------
    var wrap = document.getElementById('app');
    wrap.style.position = 'absolute';
    wrap.style.height = '100%';
    wrap.style.width = '100%';
    wrap.style.backgroundColor = '#' + r + g + b;
    var countSpanColor = document.querySelectorAll('.nameColor');
    if (countSpanColor.length == 0) {
        var nameColorCenter = document.createElement('span');
        nameColorCenter.style.position = 'absolute';
        nameColorCenter.style.zIndex = '2';
        nameColorCenter.style.top = '50%';
        nameColorCenter.style.left = '50%';
        nameColorCenter.textContent = '#' + r + g + b;
        nameColorCenter.className = 'nameColor';
        wrap.appendChild(nameColorCenter);
    } else {
        wrap.removeChild(wrap.firstChild);
        var nameColorCenter = document.createElement('span');
        nameColorCenter.style.position = 'absolute';
        nameColorCenter.style.zIndex = '2';
        nameColorCenter.style.top = '50%';
        nameColorCenter.style.left = '50%';
        nameColorCenter.textContent = '#' + r + g + b;
        nameColorCenter.className = 'nameColor';
        wrap.appendChild(nameColorCenter);
    }
};
//---getRandomIntInclusive-------
function getRandomIntInclusive(min, max) {
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max - min + 1)) + min;
};
//------button-------------------
var buttonClickOne = document.createElement('button');
buttonClickOne.style.position = 'absolute';
buttonClickOne.innerText = 'click me';
document.body.appendChild(buttonClickOne);
buttonClickOne.addEventListener('click', function() {
    app();

});