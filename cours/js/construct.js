document.addEventListener("DOMContentLoaded", function() {
    const localDataNews = localStorage.getItem('storageNewFeed');
    const localDataComent = localStorage.getItem('commentNewsFeed');
    const showNews = document.getElementById('showNews');
    const header = document.querySelector('.header');
    const construct = document.querySelector('.construct');
    const constForm = document.getElementById('constForm');
    const article = document.querySelectorAll('.article');
    const headerIn = document.getElementById('headerIn');
    let storege = [];
    let comment = [];
    // спрятать показать кнопку AddNews
    showNews.addEventListener('click', () => {
        article.forEach(e => {
            e.classList.toggle('hide');
        })
        header.classList.toggle('hide');
        construct.classList.toggle('hide');
        renderForm();
    });
    //запуск модуля внутреней формы 
    let renderForm = () => {
            constForm.innerHTML = `
        <div class="container"><form>
        <div class="row">
                <div class='col-1'>
                        <p class="constuct-autor-p">Autor</p>
                    </div>
            <div class='col-11'>
                <input type="text" class="constuct-autor" id="constuctAutorId" required minlength="3">
            </div>
        </div>
        <div class="row">
                <div class='col-1'>
                        <p>Post Text</p>
                    </div>
                <div class='col-11'>
                    <textarea  class="constuct-text" id="constuctTextId" required minlength="5"></textarea>
                </div>
            </div>
        <div class="row">
            <div class='col'>
                    <img src="" alt="" class="constuct-url-img" id="constuctImgId">
            </div>
        </div>
        <div class="row">
                <div class='col-1'>
                        <p>image ftp</p>
                    </div>
            <div class='col-5'>
                <p  class="constuct-url" id="constuctUrlId" >No files currently selected for upload</p>
            </div>
            <div class='col-6'>
                <label for="constuctBtnUrlId" class="constuct-url-btn" ><p>Загрузить файл</p></label>
                <input style="display:none;" type="file" multiple value="Загрузить файл"  id="constuctBtnUrlId" accept=".jpg, .jpeg, .png">
            </div>
        </div>
        <div class="row">
            <div class='col-1'>
                <p>image http</p>
            </div>
            <div class='col-5'>
                <input type='text'  class="constuct-http" id="constuctHttp" placeholder='No files currently selected for upload' />
            </div>
            <div class='col-6'>
                <input type="button" id="constuctBtnhttp" class="constuct-http-btn" value='Загрузить файл'/>
            </div>
        </div>
        <div class="row">
            <div class='col'>
                <input type="submit" class="constuct-submit" id="constuctSubmitId">
            </div>
        </div>
    </div>
    </form>
        `;
            // валидация формы
            let check = () => {
                const constuctAutorId = document.getElementById('constuctAutorId');
                const constuctTextId = document.getElementById('constuctTextId');
                const constuctImgId = document.getElementById('constuctImgId');
                const constuctUrlId = document.getElementById('constuctUrlId');
                const constuctBtnUrlId = document.getElementById('constuctBtnUrlId');
                const constuctSubmitId = document.getElementById('constuctSubmitId');
                const constuctHttp = document.getElementById('constuctHttp');
                const constuctBtnhttp = document.getElementById('constuctBtnhttp');
                constuctAutorId.setCustomValidity('Введите имя!');
                constuctAutorId.addEventListener("input", function() {
                    if (constuctAutorId.validity.tooShort) {
                        constuctAutorId.setCustomValidity('Введите имя!');
                    } else {
                        constuctAutorId.setCustomValidity('')
                    };
                });
                constuctTextId.setCustomValidity('Введите текст!');
                constuctTextId.addEventListener("input", function() {
                    if (constuctTextId.validity.tooShort) {
                        constuctTextId.setCustomValidity('Введите текст!');
                    } else {
                        constuctTextId.setCustomValidity('')
                    };
                });
                // получение картинки и разблокировка пути  ftp
                constuctBtnUrlId.addEventListener('change', (e) => {
                    constuctUrlId.innerText = e.target.files[0].name;
                    let fReader = new FileReader();
                    fReader.readAsDataURL(e.target.files[0]);
                    fReader.onloadend = function(event) {
                        constuctImgId.setAttribute('src', event.target.result);
                    }
                });
                // получение картинки пути  http
                constuctBtnhttp.addEventListener('click', (e) => {
                    let wayToFileHttp = constuctHttp.value;

                    function loadImageCallback(url, callback) {
                        constuctImgId.onload = function() {
                            callback(null, constuctImgId);
                            constuctHttp.style.color = '';
                        };
                        constuctImgId.onerror = function() {
                            var message = 'Error on image load at url ' + url;
                            constuctHttp.style.color = 'red';
                            callback(new Error(message));
                        };
                        constuctImgId.setAttribute('src', url);
                    }

                    function RenderAction(error, img) {
                        if (error) {
                            throw error;
                        }
                    }
                    loadImageCallback(wayToFileHttp, RenderAction);
                })
            };
            check();
            // передача данных из формы
            constuctSubmitId.addEventListener('click', (e) => {
                if (constuctAutorId.checkValidity() && constuctTextId.checkValidity()) {
                    let dat = new Date();
                    let m = dat.getMonth() + 1;
                    let date = dat.getFullYear() + '/' + m + '/' + dat.getDate() + ' ' + dat.getHours() + ':' + dat.getMinutes();
                    let id = dat.getTime();
                    let coment = 0;
                    let newNews = new News(constuctAutorId.value, constuctTextId.value, constuctImgId.src, date, 0, id, coment);
                    localStorage.setItem('storageNewFeed', JSON.stringify(storege));
                    newNews.render(headerIn);
                }

            });
        }
        //конструктор
    class News {
        constructor(autor, postText, url, datePosts, likes, id, comentNum) {
            this.comentNum = comentNum;
            this.id = id;
            this.autor = autor;
            this.postText = postText;
            this.url = url;
            this.datePosts = datePosts
            this.likes = likes;
            storege.push(this);
        }
        saveToStorage() {
            localStorage.setItem('storageNewFeed', JSON.stringify(storege));
        };
        render(target) {
            let articleOther = document.createElement('article');
            articleOther.classList.add('article');
            articleOther.innerHTML = `
            <article class='${this.id}'>
                <div class="_firstConsEl "> 
                    <div class="container">
                        <div class="row">
                            <div class="col"> 
                                <div class="news-name-autor">
                                    <h4> ${this.autor}</h4>
                                </div>
                            </div>
                            <div class="col"> 
                                <div class="news-date-autor">
                                    <p>  ${this.datePosts}</p>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col">
                                <div class="logo">
                                    <img src="${this.url}" alt="">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col">
                                <div class="text-content">
                                    <p class="text-justify">${this.postText}</p>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-8">
                                <div class="like">
                                    <button class="btn-like" aria-hidden="true" id='_btnLike'>  <span class="btn-like:hover " aria-hidden="true"> ${this.likes} </span> like </button>
                                </div>
                            </div>
                            <div class="col-2">
                                    <div class="coments">
                                        Comments <i class="_comentsnum">${this.comentNum} </i>
                                    </div>
                            </div>
                            <div class="col-2">
                                <div class="write-coments" >
                                    <button class="btn-like write" aria-hidden="true" id="_writeComents" >write coments </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <br>
                    <div class="container"><form class="_formComent hide  "> 
                        <div class="row">
                            <div class="col">
                                <input type='text' placeholder="Author" class='coment-send-autor'/>
                            </div>
                        </div>
                        <div class="row">
                                <div class="col">
                                    <textarea type='text' placeholder="Comment " class='coment-send-coment'></textarea>
                                </div>
                            </div>
                        <div class="row">
                                <div class="col">
                                    <input type='submit' class='coment-send-button' id="_sendComent" value='Send Post'/>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </article>
                    `;
            articleOther.querySelector('#_writeComents').addEventListener('click', (e) => {
                this.writeComents(articleOther, e);
            });
            articleOther.querySelector('#_btnLike').addEventListener('click', (e) => {
                this.addLike(articleOther, e);
            });
            articleOther.querySelector('#_sendComent').addEventListener('click', (e) => {
                let dat = new Date();
                let m = dat.getMonth() + 1;
                let dateComent = dat.getFullYear() + '/' + m + '/' + dat.getDate() + ' ' + dat.getHours() + ':' + dat.getMinutes();
                let formComentew = articleOther.querySelector('._formComent');
                formComentew.classList.add(this.id);
                e.preventDefault();
                let commentData = new Comment(this.id, articleOther.querySelector('.coment-send-autor').value, articleOther.querySelector('.coment-send-coment').value, dateComent);
                commentData.render(articleOther);
                localStorage.setItem('commentNewsFeed', JSON.stringify(comment));
                articleOther.querySelector('._formComent').classList.toggle('hide');
                this.addComent(articleOther);
                articleOther.querySelector('.coment-send-autor').value = '';
                articleOther.querySelector('.coment-send-coment').value = '';
            });
            target.appendChild(articleOther);
        };
        writeComents(target) {
            target.querySelector('._formComent').classList.toggle('hide');
        }
        addLike(target) {
            this.likes += 1;
            let like = target.querySelector('#_btnLike');
            like.innerHTML = 'Like ' + this.likes;
            this.saveToStorage();
        }
        addComent(target) {
            this.comentNum += 1;
            let coments = target.querySelector('._comentsnum');
            coments.innerHTML = ' ' + this.comentNum;
            this.saveToStorage();
        }
    }
    //конструктор коментариев
    class Comment {
        constructor(id, comentAutorName, comentText, dateComent) {
            this.id = id;
            this.comentAutorName = comentAutorName;
            this.comentText = comentText;
            this.dateComent = dateComent;
            comment.push(this);
        }
        render(target) {
            const firstConsEl = target.querySelector('._firstConsEl');
            let newComent = document.createElement('div');
            const formComent = target.querySelector('._formComent')
            newComent.innerHTML = `
            <div class="container">
                <span class="line-vertical"></span>
                <div class="row">  
                    <div class="col">
                        <div class="name-autor-coment">
                            <p>${this.comentAutorName}</p> 
                        </div>
                    </div>
                    <div class="col">
                        <div class="name-date-coment">
                            ${this.dateComent}
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col">
                        <div class="text-coment">
                            <p>${this.comentText}</p> 
                        </div>
                    </div>
                </div>
            </div>
            `;
            target.appendChild(newComent);
        }
    }
    //local storege
    if (localDataNews !== null) {
        let classDataNews = JSON.parse(localDataNews).map(item => {
            let classDataNews_item = new News(item.autor, item.postText, item.url, item.datePosts, item.likes, item.id, item.comentNum);
            classDataNews_item.render(headerIn);
            return classDataNews_item;
        });
        storege = classDataNews;
        if (localDataComent !== null) {
            let classData = JSON.parse(localDataComent).map(item => {
                let classDataComment = new Comment(item.id, item.comentAutorName, item.comentText, item.dateComent);
                let formComentew = document.getElementsByClassName(item.id);
                classDataComment.render(formComentew[0]);
                return classDataComment;
            });
        }
    };
});