/*

  Задание "Шифр цезаря":

    https://uk.wikipedia.org/wiki/%D0%A8%D0%B8%D1%84%D1%80_%D0%A6%D0%B5%D0%B7%D0%B0%D1%80%D1%8F

    Написать функцию, которая будет принимать в себя слово и количество
    симовлов на которые нужно сделать сдвиг внутри.

    Написать функцию дешефратор которая вернет слово в изначальный вид.

    Сделать статичные функции используя bind и метод частичного
    вызова функции (каррирования), которая будет создавать и дешефровать
    слова с статично забитым шагом от одного до 5.


    const isMobile = document.innerWidth < 768;

    Например:
      encryptCesar( 3, 'Word');
      encryptCesar1(...)
      ...
      encryptCesar5(...)

      decryptCesar1(3, 'Sdwq');
      decryptCesar1(...)
      ...
      decryptCesar5(...)

      "а".charCodeAt(); // 1072
      String.fromCharCode(189, 43, 190, 61) // ½+¾

*/


let obj = {
  e: '',
  d: '',
  s: ''
}

function encryptC(step, words) {
  let encriptArr = String(words.target.value).split('');
  var EncriptCesarArr = [];
  encriptArr.forEach(function (ar, i) {
    let EncriptCesar = ar.charCodeAt() + step;
    let decode = String.fromCharCode(EncriptCesar);
    EncriptCesarArr.push(decode);

  });
  divEncript.innerText = EncriptCesarArr.join('');
  obj.e = divEncript.innerText;
};

function decryptC(step, words) {
  let encriptArr = String(words.target.value).split('');
  var EncriptCesarArr = [];

  encriptArr.forEach(function (ar, i) {
    let EncriptCesar = ar.charCodeAt() - step;
    let decode = String.fromCharCode(EncriptCesar);
    EncriptCesarArr.push(decode);
  });
  divDecript.innerText = EncriptCesarArr.join('');
  obj.d = divDecript.innerText;
}
let createStep = document.createElement('input');
let createLableStep = document.createElement('lable');
createLableStep.textContent = 'choose number step';
createStep.value = 3;
obj.s = Number(createStep.value);
createStep.addEventListener('input', function (e) {
  obj.s = Number(e.target.value);
});
var stepEncript = encryptC.bind(null);
var stepDecript = decryptC.bind(null);
let createInputEncript = document.createElement('input');
let createLableEncript = document.createElement('lable');
let divEncript = document.createElement('div');
createLableEncript.textContent = ' input text encrept';
createInputEncript.classList.add('encript');
createInputEncript.addEventListener('input', function (e) {
  stepEncript(obj.s, e)
});
let createInputDecript = document.createElement('input');
let createLableDecript = document.createElement('lable');
let divDecript = document.createElement('div');
createLableDecript.textContent = ' input text Decript';
createInputDecript.classList.add('decript');
createInputDecript.addEventListener('input', function (e) {
  stepDecript(obj.s, e)
});
document.body.appendChild(createStep);
document.body.appendChild(createLableStep);
document.body.appendChild(createInputEncript);
document.body.appendChild(createLableEncript);
document.body.appendChild(divEncript);
document.body.appendChild(createInputDecript);
document.body.appendChild(createLableDecript);
document.body.appendChild(divDecript);