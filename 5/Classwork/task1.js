/*

    Задание 1:

    Написать обьект Train у которого будут свойства:
    -имя,
    -скорость езды
    -количество пассажиров
    Методы:
    Ехать -> Поезд {name} везет { количество пассажиров} со скоростью {speed}
    Стоять -> Поезд {name} остановился. Скорость {speed}
    Подобрать пассажиров -> Увеличивает кол-во пассажиров на Х
*/
"use strict";
var Train = {};
Train.name = 'Tr-1';
Train.speed = '120';
Train.passenger = 500;
Train.goTrein = function () {
    console.log('Поезд', this.name, 'везет', this.passenger, 'пасажиров со скоростью', this.speed);

};
Train.stopTrein = function () {
    console.log('Поезд', this.name, 'остановился', this.speed = '0');

};
Train.putPassenger = function (x) {
    console.log('подобрать пасажиров', this.passenger = (this.passenger + x));

};
console.log(Train.goTrein());
console.log(Train.stopTrein());
console.log(Train.putPassenger(2));