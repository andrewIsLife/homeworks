/*

    Задание 2.

    Напишите фунцию, которая изменяет цвет-фона и цвет текста, присваивая к новым цветам
    значение из this.color, this.background
    А так же добавляет элемент h1 с текстом "I know how binding works in JS"

    1.1 Ф-я принимает один аргумент,
    второй попадает в него через метод .call(obj)

    1.2 Ф-я не принимает никаких аргументов,
    а необходимые настройки полностью передаются через bind

    1.3 Ф-я принимает фразу для заголовка,
    обьект с настройками передаем через .apply();

*/
let colors = {
  background: 'purple',
  color: 'white',
  h1: 'I know how binding works in JS'
}
//call
function myCall(color) {
  document.body.style.background = this.background;
  document.body.style.color = color;
  let textElement = document.createElement('h1');
  textElement.innerText = this.h1;
  document.body.appendChild(textElement);
}

let buttonCall = document.createElement('button');
buttonCall.innerText = 'call';
document.body.appendChild(buttonCall);
buttonCall.addEventListener('click', function () {
  myCall.call(colors, 'red')
});
//bind
function myBind() {
  document.body.style.background = this.background;
  document.body.style.color = this.color;
  let textElement = document.createElement('h1');
  textElement.innerText = this.h1;
  document.body.appendChild(textElement);
}

let buttonBind = document.createElement('button');
buttonBind.innerText = 'bind';
document.body.appendChild(buttonBind);
let bindMyBind = myBind.bind(colors);
buttonBind.addEventListener('click', bindMyBind);
//apply
function myApply(text) {
  document.body.style.background = this.background;
  document.body.style.color = this.color;
  let textElement = document.createElement('h1');
  textElement.innerText = text;
  document.body.appendChild(textElement);
}

let buttonApply = document.createElement('button');
buttonApply.innerText = 'apply';
document.body.appendChild(buttonApply);
buttonApply.addEventListener('click', function () {
  myApply.apply(colors, ['I know how binding works in JS']);
});