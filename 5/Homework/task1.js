/*

  Задание:

    1. Написать конструктор объекта комментария который принимает 3 аргумента
    ( имя, текст сообщения, ссылка на аватаку);

    {
      name: '',
      text: '',
      avatarUrl: '...jpg'
      likes: 0
    }
      + Создать прототип, в котором будет содержаться ссылка на картинку по умлочанию
      + В прототипе должен быть метод который увеличивает счетик лайков

    var myComment1 = new Comment(...);

    2. Создать массив из 4х комментариев.
    var CommentsArray = [myComment1, myComment2...]

    3. Созадть функцию конструктор, которая принимает массив коментариев.
      И выводит каждый из них на страничку.

    <div id="CommentsFeed"></div>


*/
"use strict";

function Comment(name, text, avatarUrl) {
  this.name = name;
  this.text = text;
  this.avatarUrl = avatarUrl + '.jpg';
}
let CommentsArray = [];
let name = document.getElementById('name');
let comment = document.getElementById('comment');
let avatarUrl = document.getElementById('url');
let like = document.getElementById('like');
let like_label = document.getElementById('like_label');
let showComments = document.getElementById('showComments');
let exportArr = document.getElementById('exportArr');
let add_coments = document.getElementById('add_coments');
let myComment = new Comment(name.value, comment.value, url.value);
let likeObj = {
  likes: 0
};
CommentsArray.push(myComment);
Object.setPrototypeOf(myComment, likeObj);

function preLikeOP() {
  let y = CommentsArray.pop(Comment);
  y.name = name.value;
  y.text = comment.value;
  y.avatarUrl = avatarUrl.value;
  y.likes += 1;
  like_label.innerText = y.likes;
  CommentsArray.push(y);
};
like.addEventListener('click', preLikeOP.bind(CommentsArray));
showComments.addEventListener('click', () => {
  let y = CommentsArray.pop(Comment);
  y.name = name.value;
  y.text = comment.value;
  y.avatarUrl = avatarUrl.value;
  CommentsArray.push(y);
  CommentsArray.forEach((element, i, arr) => {
    let p = document.createElement('p');
    p.innerHTML = `${' Number Arr '+ i +', name '+ element.name+', comment '+ element.comment+', avatarUrl '+ element.avatarUrl+', likes '+ element.likes}`;
    exportArr.appendChild(p);
  });
});
let buttonAddComent = add_coments.addEventListener('click', function () {
  like_label.innerText = 0;
  let p = document.querySelectorAll('p');
  p.forEach(e => {
    exportArr.removeChild(e);
  })
  if (likeObj.likes > 0) {
    likeObj.likes = 0;
  }
  let myComment2 = new Comment(name.value, comment.value, url.value);
  CommentsArray.push(myComment2);
  Object.setPrototypeOf(myComment2, likeObj);
})