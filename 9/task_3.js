let storege = [];
const root = document.getElementById('root');

class Data {
    constructor({ name, id = new Date().getTime(), description, likes }) {
        this.name = name;
        this.id = id;
        this.description = description;
        this.likes = likes;
        storege.push(this);
        this.saveToStorage();
    }
    saveToStorage() {
        localStorage.setItem('storage', JSON.stringify(storege));
    };
    render(target) {
        let node = document.createElement('div');
        node.innerHTML = `
        <input class='_name' placeholder='Your name' value='${this.name}'></input>
        <br>
        <input class='_message' placeholder='message' value='${this.description}'></input>
        <br>
        <button class='_like'>Like ${this.likes}</button>
        `;
        node.querySelector('._name').addEventListener('input', (e) => {
            this.addName(node, e);
        });
        node.querySelector('._message').addEventListener('input', (e) => {
            this.addMassege(node, e);
        });
        node.querySelector('._like').addEventListener('click', () => {
            this.addLikes(node);
        });
        target.appendChild(node);
    }
    addName(target, e) {
        this.name = e.target.value;
        this.saveToStorage();
    };
    addMassege(target, e) {
        this.description = e.target.value;
        this.saveToStorage();
    }
    addLikes(target) {
        this.likes += 1;
        let like = target.querySelector('._like');
        like.innerHTML = 'Like ' + this.likes;
        this.saveToStorage();
    }
}


const localData = localStorage.getItem('storage');
if (localData !== null) {
    let classData = JSON.parse(localData).map(item => {
        cl_item = new Data(item);
        cl_item.render(root);
        return cl_item;
    });
    storege = classData;
};

let x = new Data({ name: '', description: '', likes: 0 });
x.render(root);


console.log(storege);