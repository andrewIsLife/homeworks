/*

    Задание 1:
    Написать скрипт, который по клику на кнопку рандомит цвет и записывает его в localStorage
    После перезагрузки страницы, цвет должен сохранится.

*/

let button = document.getElementById('btn-0');
let body = document.querySelector('body');
let background = localStorage.getItem("color");
if (background !== null) {
    document.body.style.backgroundColor = background;
}

function getRandomIntInclusive(min, max) {
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max - min + 1)) + min;
};
button.addEventListener('click', function() {
    let random = '#' + getRandomIntInclusive(0, 255) + '' + getRandomIntInclusive(0, 255) + '' + getRandomIntInclusive(0, 255);
    var color = localStorage.setItem('color', random);
});